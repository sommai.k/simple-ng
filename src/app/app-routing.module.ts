import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GbhrListComponent } from './page/gbhr-list/gbhr-list.component';
import { GbhrComponent } from './page/gbhr/gbhr.component';
import { HomeComponent } from './page/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'gbhr',
    component: GbhrComponent,
  },
  {
    path: 'gbhr-list',
    component: GbhrListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
