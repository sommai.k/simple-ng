import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbhrListComponent } from './gbhr-list.component';

describe('GbhrListComponent', () => {
  let component: GbhrListComponent;
  let fixture: ComponentFixture<GbhrListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbhrListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbhrListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
