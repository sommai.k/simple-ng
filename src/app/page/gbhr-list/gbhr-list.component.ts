import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { GbhrOutput, GbhrService } from 'src/app/shared/gbhr.service';

@Component({
  selector: 'app-gbhr-list',
  templateUrl: './gbhr-list.component.html',
  styleUrls: ['./gbhr-list.component.scss'],
})
export class GbhrListComponent implements OnInit {
  displayedColumns = ['code', 'name', 'title'];
  constructor(public service: GbhrService) {}

  ngOnInit(): void {}

  onPageChange(event: PageEvent) {
    this.service.load(event.pageIndex, event.pageSize);
  }
}
