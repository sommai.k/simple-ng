import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { GbhrService } from 'src/app/shared/gbhr.service';

@Component({
  selector: 'app-gbhr',
  templateUrl: './gbhr.component.html',
  styleUrls: ['./gbhr.component.scss'],
})
export class GbhrComponent implements OnInit {
  gbhrForm: FormGroup;
  titles: string[] = ['นักพัฒนา', 'เจ้าหน้าที่', 'การเงิน'];

  constructor(private fb: FormBuilder, private service: GbhrService) {
    this.gbhrForm = fb.group({
      code: ['', [Validators.required, Validators.minLength(5)]],
      name: [''],
      title: ['นักพัฒนา'],
    });
  }

  ngOnInit(): void {}

  onFormSubmit() {
    if (this.gbhrForm.valid) {
      this.service.save(this.gbhrForm.value).subscribe({
        next: (resp) => console.log(resp),
        error: (err) => console.log(err),
      });

      // this.service.save(this.gbhrForm.value).subscribe(
      //    (resp) => console.log(resp),
      //    (err) => console.log(err),
      // );
    } else {
      console.log('please validate form');
    }
  }
}
