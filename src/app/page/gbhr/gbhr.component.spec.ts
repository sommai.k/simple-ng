import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbhrComponent } from './gbhr.component';

describe('GbhrComponent', () => {
  let component: GbhrComponent;
  let fixture: ComponentFixture<GbhrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbhrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbhrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
