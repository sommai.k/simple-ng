import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  appName: string = 'Home Component';

  constructor() {}

  ngOnInit(): void {}

  onFormSubmit(ngForm: NgForm) {
    if (ngForm.valid) {
      console.log('Valid form');
    } else {
      console.log('Invalid form');
    }
  }
}
