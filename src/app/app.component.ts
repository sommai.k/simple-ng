import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title: string = 'simple-ng';
  subTitle: string = 'this is a sub title';
  info: Info = {
    title: 'Simple Angular',
    subTitle: 'Description',
  };

  url: string = 'http://www.google.com';

  constructor(private router: Router) {}

  showMessage() {
    this.title = 'test change title value';
    this.url = 'http://www.yahoo.com';
  }

  goHomePage() {
    this.router.navigate(['home']);
  }

  goGbhrPage() {
    this.router.navigate(['gbhr']);
  }

  goGbhrListPage() {
    this.router.navigate(['gbhr-list']);
  }
}

export interface Info {
  title: string;
  subTitle: string;
}
