import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class GbhrService {
  gbhrs: GbhrOutput[] = [];

  constructor(private http: HttpClient) {
    this.load(0, 10);
  }

  save(data: GbhrInput) {
    const url = environment.api_host + '/gbhr';
    return this.http.post(url, data);
  }

  loadAll(start: number, size: number) {
    const url = `${environment.api_host}/gbhr?start=${start}&size=${size}`;
    return this.http.get<GbhrOutput[]>(url);
  }

  load(start: number, size: number) {
    this.loadAll(start, size).subscribe({
      next: (resp) => (this.gbhrs = resp),
      error: (err) => console.log(err),
    });
  }
}

export interface GbhrInput {
  code: string;
  name: string;
  title: string;
}

export interface GbhrOutput extends GbhrInput {
  id: number;
}
