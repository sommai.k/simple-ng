import { TestBed } from '@angular/core/testing';

import { GbhrService } from './gbhr.service';

describe('GbhrService', () => {
  let service: GbhrService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GbhrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
